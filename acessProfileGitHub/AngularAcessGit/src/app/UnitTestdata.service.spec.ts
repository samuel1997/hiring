/*
author: Samuel Silva
github: github.com/samuel199732 
*/
// import modules
import { TestBed, inject } from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { UnitTestdata } from './UnitTestdata.service';
import {User} from './User';

let service: UnitTestdata;
let httpMoch:HttpTestingController;

describe('DataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UnitTestdata]
    });
    service = TestBed.get(UnitTestdata);
    httpMoch = TestBed.get(HttpTestingController);
  });
  afterEach(()=>{
    httpMoch.verify();
  })
  it('should retrieve user from the server via post', () => {
    const userPost:User[] = [{
      avatar_url:'https://avatars1.githubusercontent.com/u/16599204?v=4',
      name: 'Samuel Silva',
      login: 'samuel199732',
      followers:0,
      following:1,
      blog:'Not available'
    }];
    service.callToAjax().subscribe(User => {
      expect(User.length).toBe(1);
      expect(User).toEqual(userPost);
    });
    const request = httpMoch.expectOne(`${service.ROOT_URL}`);
    expect(request.request.method).toBe('POST');
    request.flush(userPost);
  });

});
