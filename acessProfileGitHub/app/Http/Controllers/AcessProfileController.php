<?php
/*
author: Samuel Silva
github: github.com/samuel199732 
This controller controls all of my views and models. In this case, as it is the first version, 
it contains only two functions. calltoView (), 
which calls our view and seachUserName () 
which is called by the angular request in javascript.
*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\WebRequest;
class AcessProfileController extends Controller
{
    //
    public function calltoView()
    {
        return view('acess.acessProfileView'); // cal to main view
    }
    public function seachUserName(Request $request)
    {
        $user = $request->search; 
        if(!empty($user)){
            $url = "https://api.github.com/users/".$user;
            $result =  WebRequest::getData($url, "" ,"GET"); // calls the class that will request the API. This file is located in App / Helper / WebRequest.php
        }
        else {
            // if the request comes empty
            $result = [
                'message'=> 'field is empty',
                'status'=> 400
            ];
            
        }
        return $result; // json returns
    }
}
