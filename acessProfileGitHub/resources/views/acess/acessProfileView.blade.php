<!doctype html>
<html lang="en">
  <!--author: Samuel Silva
github: github.com/samuel199732 
html HTML generated by angular -->
<head>
  <meta charset="utf-8">
  <title>Github Profile</title>
  <base href="/">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="https://cdn.iconscout.com/public/images/icon/free/png-128/github-octocat-logo-brand-development-tools-3d039a67a47aa726-128x128.png">
</head>
<body>
<app-root></app-root>
<script type="text/javascript" src="js/inline.bundle.js"></script>
<script type="text/javascript" src="js/polyfills.bundle.js"></script>
<script type="text/javascript" src="js/styles.bundle.js"></script>
<script type="text/javascript" src="js/vendor.bundle.js"></script>
<script type="text/javascript" src="js/main.bundle.js"></script></body>
</html>