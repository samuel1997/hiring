<?php
/*
author: Samuel Silva
github: github.com/samuel199732 
class connecting to github API
this file is called by the AccessProfileController controller, 
which is located in \ App \ Http \ Controllers \ AcessProfileController.php
*/

namespace App\Helper;
class WebRequest{
    public static function getData($url, $data, $kindofConect){        
        $curl = curl_init();
        /* initial setup, we need to send in the 'User-Agent' 
        header for API to know who we are. Otherwise we will take 404
        */
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $kindofConect,
          CURLOPT_POSTFIELDS => $data,
          CURLOPT_HTTPHEADER => array(
            "User-Agent: samuel199732",
            "cache-control: no-cache",
            "content-type: application/json",
          ),
        ));
        // API connection
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        return $response;
    }
}