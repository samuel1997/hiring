/*
author: Samuel Silva
github: github.com/samuel199732 
*/
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {User} from './User';
@Injectable()
export class UnitTestdata {
  // url server
  ROOT_URL = 'api/search';
  constructor(private http: HttpClient) {} // constructor define http
  body = {
    'search':'samuel199732'
   }; // body
  callToAjax(){
    return this.http.post<User[]>(`${this.ROOT_URL}`, this.body); // request to server
  }
}
