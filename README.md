## How to install project ##
To run this project you will need to install 3 applications. Docker, laravel and angular. Here's how to install these apps.

### Installing docker ###
But first, let's update the package database:

````
 sudo apt-get update
````
Now, let's install Docker. Add the official GPG key from the Docker repository to the system:
 
````
 sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
 ````
 Make sure that you are installing from the Docker repository instead of the default Ubuntu repository
 ````
 apt-cache policy docker-engine
 ````
 You should see output similar to the following:
 ````
 docker-engine:
  Installed: (none)
  Candidate: 1.11.1-0~xenial
  Version table:
     1.11.1-0~xenial 500
        500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
     1.11.0-0~xenial 500
        500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
````
Note that the docker-engine is not installed, but the candidate for installation is from the Ubuntu Docker repository. The version number of the docker-engine may be different.
  
Finally, install Docker:

````
sudo apt-get install -y docker-engine
````
Docker will now be installed, the daemon started, and the process enabled to boot. Verify that it is running

````
sudo systemctl status docker
````
The output should look similar to the following, showing that the service is up and running:

````
[ secondary_label Output]
● docker.service - Docker Application Container Engine
   Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
   Active: active (running) since Sun 2016-05-01 06:53:52 CDT; 1 weeks 3 days ago
     Docs: https://docs.docker.com
 Main PID: 749 (docker)
 ````
 
### Installing PHP ###
For ubuntu >= 16.4
````
sudo apt-get install php
````
### Installing Composer ###

Before installing laravel you will need to install the composer, so let's install
To quickly install Composer in the current directory, run the following script in your terminal
````
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
````

### Installing Laravel ###
Server Requirements
The Laravel framework has a few system requirements. Of course, all of these requirements are satisfied by the Laravel Homestead virtual machine, so it's highly recommended that you use Homestead as your local Laravel development environment.

However, if you are not using Homestead, you will need to make sure your server meets the following requirements:

- PHP >= 7.0.0
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

To see the version

````
php -v
````

First, download the Laravel installer using Composer

````
composer global require "laravel/installer"
````
### Installing Angular ###
You need to set up your development environment before you can do anything.
Install Node.js® and npm if they are not already on your machine.

#### Installing Node.js and npm ####
We will need curl to add node.js ppa in our system. In addition we will also need to install python-software-properties package if its is not installed already.We will need curl to add node.js ppa in our system. In addition we will also need to install python-software-properties package if its is not installed already.
````
sudo apt-get install curl
sudo apt-get install python-software-properties
````
Adding NodeJs PPA
````
$ curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
````
After adding the node.js ppa we just need to give the node.js install command to install node.js. Npm will be installed automatically with node.js.

````
$ sudo apt-get install nodejs
````

Check installed Node.js version by following command

````
$ node -v 

v6.5.0
````
Check installed npm version by following command
````
$ npm -v 

3.10.3
````
Then install the Angular CLI globally.
````
npm install -g @angular/cli
````
## git repository ##
Clone repository with git clone command.This link is for example only
````
git clone git://github.com/schacon/grit.git 
````
after that in the project root folder /hiring. type it
````
sudo docker-compose up
````
If there was no error the project is already running in the link: localhost: 8000

# What is this project? #
Basically this project is an application in which you can search your nickname in github. It will show you some information about this nicknam
