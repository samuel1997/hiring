<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
author: Samuel Silva
github: github.com/samuel199732
route file is where I define my api routes. 
I have decided to leave here because the api route has less middleware to be passed. 
This prevents for example that my angular ajax request receives the 401 server error. 
That is the unauthorized error.

*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => ['api']], function(){
    Route::post('search', 'AcessProfileController@seachUserName');
    Route::get('search', function(){return redirect('home');});
});
