/*author: Samuel Silva
github: github.com/samuel199732 */
// user model
export interface User{
    avatar_url:string;
    name:string;
    login: string;  
    followers:number;
    following:number;
    blog:string;
}