webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<nav-bar></nav-bar>\n<content></content>\n<app-footer></app-footer>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__nav_bar_nav_bar_component__ = __webpack_require__("../../../../../src/app/nav-bar/nav-bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__content_content_component__ = __webpack_require__("../../../../../src/app/content/content.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__footer_footer_component__ = __webpack_require__("../../../../../src/app/footer/footer.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["G" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_5__nav_bar_nav_bar_component__["a" /* NavBarComponent */],
                __WEBPACK_IMPORTED_MODULE_6__content_content_component__["a" /* ContentComponent */],
                __WEBPACK_IMPORTED_MODULE_8__footer_footer_component__["a" /* FooterComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_common_http__["b" /* HttpClientModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/content/content.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*component CSS was imported from bootstrap class 4. \nwith some enhancements and addiction\nview link : https://getbootstrap.com/docs/4.0/getting-started\nauthor: Samuel Silva\ngithub: github.com/samuel199732\n*/\n /* image background, this image will be at the top of the application */\n.bg-img{\n    position: absolute;\n    left: 0;\n    top: 0;\n    right: 0;\n    bottom: 0;\n    z-index: -1;\n    background-position: center;\n    background-size: cover;\n    background-attachment: fixed;\n    max-width: 100%;\n  }\n  /*Overlay effect, this effect will overlap the background image with an opacity giving a quite interesting effect */\n  .bg-img .overlay {\n    position: absolute;\n    left: 0;\n    top: 0;\n    right: 0;\n    bottom: 0;\n    opacity: .8;\n    background: #1C1D21;\n}\n/* formatting input search */\n.search-box input {\n    font-family: Arial, Helvetica, sans-serif;\n    color: black;\n    background: white;\n    border:0;\n    outline: none !important;\n    transition: all 0.2s linear;\n    width:80%; \n    margin-left:auto; \n    margin-right:auto;\n    text-align: center\n}\n/* formatting when the field gains focus*/\n.search-box input:focus {\n    background: #fff !important;\n    outline: 0;\n    color: black;\n}\n/*formatting when the mouse rolls over*/\n.search-box input:hover {\n    background: rgb(240, 233, 233);\n}\n/*header's height */\nheader{\n  height: 78vh;\n}\n  /* Absolute Center Spinner */\n  .loading {\n    position: fixed;\n    z-index: 999;\n    height: 2em;\n    width: 2em;\n    overflow: show;\n    margin: auto;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    right: 0;\n  }\n  \n  /* Transparent Overlay */\n  .loading:before {\n    content: '';\n    display: block;\n    position: fixed;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background-color: rgba(0,0,0,0.3);\n  }\n  \n  /* :not(:required) hides these rules from IE9 and below */\n  .loading:not(:required) {\n    /* hide \"loading...\" text */\n    font: 0/0 a;\n    color: transparent;\n    text-shadow: none;\n    background-color: transparent;\n    border: 0;\n  }\n  /* formatting when loading*/\n  .loading:not(:required):after {\n    content: '';\n    display: block;\n    font-size: 10px;\n    width: 1em;\n    height: 1em;\n    margin-top: -0.5em;\n    -webkit-animation: spinner 1500ms infinite linear;\n    animation: spinner 1500ms infinite linear;\n    border-radius: 0.5em;\n    box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;\n  }\n  \n  /* Animation */\n  \n  @-webkit-keyframes spinner {\n    0% {\n      -webkit-transform: rotate(0deg);\n      transform: rotate(0deg);\n    }\n    100% {\n      -webkit-transform: rotate(360deg);\n      transform: rotate(360deg);\n    }\n  }\n  @keyframes spinner {\n    0% {\n      -webkit-transform: rotate(0deg);\n      transform: rotate(0deg);\n    }\n    100% {\n      -webkit-transform: rotate(360deg);\n      transform: rotate(360deg);\n    }\n  }\n  /* media query improving responsiveness */\n  @media screen and (min-height: 550px) {\n    #divContent{\n      text-align: center; \n      padding:0px; \n      margin-top:100px;\n    }\n    #divContent img {\n      margin:60px;\n      width:200px;\n    }\n  \n  }\n  @media screen and (max-height: 550px) {\n    #divContent{\n      text-align: center; \n      padding:0px; \n      margin-top:50px;\n    }\n    #divContent img {\n      margin:10px;\n      width:125px;\n    }\n  }\n  @media screen and (max-height: 240px) {\n    #divContent{\n      text-align: center; \n      padding:0px; \n      margin-top:50px;\n    }\n    #divContent img {\n      margin:10px;\n      width:50px;\n    }\n  }", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/content/content.component.html":
/***/ (function(module, exports) {

module.exports = "<!--author: Samuel Silva\ngithub: github.com/samuel199732 -->\n<header>\n  <!-- fixed background image -->\n    <div class=\"bg-img\" style=\"background-image:url('http://yfkemener.com/wp-content/uploads/2016/10/Cover-Productions.jpg');\">\n       <!-- overlapping effect -->\n      <div class=\"overlay\">  </div>\n    </div>\n<div class=\"search-box\" >\n    <div class=\"col-xs-12 col-sm-12 col-md-12\" id=\"divContent\">\n      <!--logo -->\n    <img id=\"logo\" src=\"https://images.sftcdn.net/images/t_optimized,f_auto/p/341c5968-96d8-11e6-bbd4-00163ec9f5fa/1961917216/github-for-windows-logo.png\"  alt=\"picture description\"  >\n    <!-- inpu text -->\n    <input (focus)=\"onHidden()\" id=\"search\" style=\"height:45px;\" name=\"search\" value=\"\" type=\"text\" class=\"form-control\" [(ngModel)] = \"search\"/>\n    <!-- input button -->\n    <button [disabled]=\"!search || load\" class=\"btn btn-secondary\" style=\" text-align: center; padding: 10px; margin:15px\" (click)=\"callToAjax(search)\" type=\"submit\">Search</button>\n    </div>\n  </div>\n</header>\n<!-- here we use ng-if. ng-f compares if a condition is true. \n  To check the control of these attributes check: content.component.ts -->\n  <div *ngIf='search && show' class=\"card card-body\" style=\"border:none; margin:20vh;background-color: #e7e7e7; width:75%; margin-right:auto; margin-left:auto; display:block\" >\n    <div>\n          <img class=\"img-thumbnail\" (load)='finishLoad()' style=\"display: block; margin-left: auto; margin-right: auto; margin-bottom: 25px; margin-top: 10px;\" src=\"{{user.avatar_url}}\" width=\"200\" alt=\"\">\n          <!-- here we use ng-if. ng-f compares if a condition is true. \n              To check the control of these attributes check: content.component.ts.\n            if loading image -->\n          <div *ngIf='load'>\n          <div style=\"text-align:center; width:75%; margin-left: auto; margin-right: auto;\" class=\"\"><div class=\"card-header\"><span style=\"font-family: 'Abril Fatface', cursive;\" class='user-label'>Name:</span></div><div class=\"card-block\">{{user.name}}</div></div>\n          <div style=\"text-align:center; width:75%; margin-left: auto; margin-right: auto;\" class=\"\"><div class=\"card-header\"><span style=\"font-family: 'Abril Fatface', cursive;\" class='user-label'>Login:</span></div><div class=\"card-block\"> {{user.login}}</div></div>\n          <div style=\"text-align:center; width:75%; margin-left: auto; margin-right: auto;\" class=\"\"><div class=\"card-header\"><span style=\"font-family: 'Abril Fatface', cursive;\" class='user-label'>Blog:</span></div><div class=\"card-block\">{{user.blog}}</div></div>\n          <div style=\"text-align:center; width:75%; margin-left: auto; margin-right: auto;\" class=\"\"><div class=\"card-header\"><span style=\"font-family: 'Abril Fatface', cursive;\" class='user-label'>Followers:</span></div><div class=\"card-block\"> {{user.followers}}</div></div>\n          <div style=\"text-align:center; width:75%; margin-left: auto; margin-right: auto;\" class=\"\"><div class=\"card-header\"><span style=\"font-family: 'Abril Fatface', cursive;\" class='user-label'>Following:</span></div><div class=\"card-block\"> {{user.following}}</div></div>\n          </div>\n          <!--here we use ng-if. ng-f compares if a condition is true. \n              To check the control of these attributes check: content.component.ts\n            if image has not yet uploaded  -->\n          <div *ngIf=\"!load\" class=\"loading\"></div>\n    </div>\n  </div>\n  \n  "

/***/ }),

/***/ "../../../../../src/app/content/content.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*this class contains behavior of the application content
author: Samuel Silva
github: github.com/samuel199732
*/


var ContentComponent = (function () {
    // constructor define http
    function ContentComponent(http) {
        this.http = http;
        // attributes that will be used to control the form via * ngIf . 
        //To check the conditions that control the behavior of inputs check: content.component.html
        this.login = '';
        this.show = false;
        this.load = false;
        // user model
        this.user = {
            avatar_url: '',
            name: '',
            login: '',
            followers: 0,
            following: 0,
            blog: ''
        };
    }
    // Function that is called when button is clicked
    ContentComponent.prototype.callToAjax = function ($event) {
        this.login = $event;
        this.onShow();
        this.ajax();
    };
    ;
    // when the image finishes loading
    ContentComponent.prototype.finishLoad = function () {
        this.load = true;
    };
    // function that requests the server
    ContentComponent.prototype.ajax = function () {
        var _this = this;
        // request body
        var body = {
            'search': this.login
        };
        this.http.post("api/search", body).subscribe(function (data) {
            if (!data['message']) {
                _this.user.login = data['login'];
                _this.user.avatar_url = data['avatar_url'];
                _this.user.name = data['name'] ? data['name'] : 'Not available';
                _this.user.followers = data['followers'];
                _this.user.following = data['following'];
                _this.user.blog = data['blog'] ? data['blog'] : 'Not available';
            }
            else {
                // if there were any errors. I show not found
                _this.user.login = 'Not found',
                    _this.user.avatar_url = 'https://avatars2.githubusercontent.com/u/2826843?v=4',
                    _this.user.name = 'Not found',
                    _this.user.followers = 0,
                    _this.user.following = 0,
                    _this.user.blog = 'Not found';
            }
        });
    };
    // function that shows panel. 
    ContentComponent.prototype.onShow = function () { this.show = true; };
    // function that hidden panel. 
    ContentComponent.prototype.onHidden = function () {
        this.show = false;
        this.load = false;
        this.user.avatar_url = '';
    };
    //  function that is called at startup
    ContentComponent.prototype.ngOnInit = function () {
        {
            this.login = '';
            this.show = false;
            this.load = false;
        }
    };
    ContentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'content',
            template: __webpack_require__("../../../../../src/app/content/content.component.html"),
            styles: [__webpack_require__("../../../../../src/app/content/content.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], ContentComponent);
    return ContentComponent;
}());



/***/ }),

/***/ "../../../../../src/app/footer/footer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*/*component's CSS was imported from bootstrap class 4\nview link : https://getbootstrap.com/docs/4.0/getting-started\nauthor: Samuel Silva\ngithub: github.com/samuel199732\n*/\n.container p{\n    text-align: center;\n    font-size: 14px;\n    text-transform: uppercase;\n    margin: 0;\n}\n.list-inline{\n    margin: 10px;\n    \n}\n.list-inline li{\n    padding: 5px;\n\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<!--author: Samuel Silva\ngithub: github.com/samuel199732 -->\n    <footer class=\"py-5 bg-dark\">\n            <!-- fontawesome site loader icons. for more information check fontawesome.io/icons/ -->\n      <ul class=\"list-inline\" style=\"text-align:center\">\n        \n          <li class=\"list-inline-item\"><a href=\"#\" target=\"_blank\"><i style=\"color:white;\" class=\"fa fa-facebook fa-2x\"></i></a></li>\n          <li class=\"list-inline-item\"><a href=\"#\" target=\"_blank\"><i style=\"color:white;\" class=\"fa fa-twitter fa-2x\"></i></a></li>\n          <li class=\"list-inline-item\" ><a href=\"https://www.linkedin.com/in/samuel-silva-6b4761101/\" target=\"_blank\"><i style=\"color:white;\" class=\"fa fa-linkedin fa-2x\"></i></a></li>\n          <li class=\"list-inline-item\"><a href=\"https://github.com/samuel199732\" target=\"_blank\"><i style=\"color:white;\" class=\"fa fa-github fa-2x\"></i></a></li>\n      </ul>\n      <div class=\"container\">\n        <p class=\"m-0 text-center text-white\">COPYRIGHT &copy; 2017. ALL RIGHTS RESERVED. GITHUB PROFILE 2017</p>\n      </div>\n\n    </footer>"

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'app-footer',
            template: __webpack_require__("../../../../../src/app/footer/footer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/nav-bar/nav-bar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*component's CSS was imported from bootstrap class 4\nview link : https://getbootstrap.com/docs/4.0/getting-started\n*/\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/nav-bar/nav-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<!--author: Samuel Silva\ngithub: github.com/samuel199732 -->\n    <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark fixed-top\">\n      <div class=\"container\">\n        <a class=\"navbar-brand\" href=\"#\">Github Profile</a>\n        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n          <span class=\"navbar-toggler-icon\"></span>\n        </button>\n        <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">\n          <ul class=\"navbar-nav ml-auto\">\n            <li class=\"nav-item active\">\n              <a class=\"nav-link\" href=\"#\">Home\n                <span class=\"sr-only\">(current)</span>\n              </a>\n            </li>\n            <li class=\"nav-item\">\n              <a class=\"nav-link\" href=\"https://github.com/samuel199732\" target=\"_blank\">Contact</a>\n            </li>\n          </ul>\n        </div>\n      </div>\n    </nav>\n\n"

/***/ }),

/***/ "../../../../../src/app/nav-bar/nav-bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavBarComponent = (function () {
    function NavBarComponent() {
    }
    NavBarComponent.prototype.ngOnInit = function () {
    };
    NavBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'nav-bar',
            template: __webpack_require__("../../../../../src/app/nav-bar/nav-bar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/nav-bar/nav-bar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NavBarComponent);
    return NavBarComponent;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_10" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map