/*this class contains behavior of the application content
author: Samuel Silva
github: github.com/samuel199732
*/
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {User} from './../User';
@Component({
  selector: 'content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  
  // attributes that will be used to control the form via * ngIf . 
  //To check the conditions that control the behavior of inputs check: content.component.html
  login = '';
  show = false;
  load = false;

 // user model
  user:User = {
    avatar_url:'',
    name:'',
    login:'',
    followers:0,
    following:0,
    blog: ''
  };
  // constructor define http
  constructor(private http: HttpClient) {}
  // Function that is called when button is clicked
  callToAjax($event){
    this.login = $event; 
    this.onShow();
    this.ajax();
  };
  // when the image finishes loading
  finishLoad(){
    this.load = true;
  }
  // function that requests the server
  ajax():void{
    // request body
    var body = {
      'search':this.login
     };
      this.http.post("api/search",body ).subscribe(data => { // request to server

        if(!data['message']){ // If the request was successful and there is this user. obs: any error they give it will return with this message parameter
          this.user.login = data['login'];
          this.user.avatar_url= data['avatar_url'];
          this.user.name = data['name'] ? data['name'] : 'Not available';
          this.user.followers= data['followers'];
          this.user.following = data['following'];
          this.user.blog = data['blog'] ? data['blog'] : 'Not available';
        }else{
          // if there were any errors. I show not found
          this.user.login = 'Not found',
          this.user.avatar_url= 'https://avatars2.githubusercontent.com/u/2826843?v=4',
          this.user.name = 'Not found',
          this.user.followers= 0,
          this.user.following = 0,
          this.user.blog = 'Not found'
        }
      });
  }
  // function that shows panel. 
  onShow() { this.show = true; } 
  // function that hidden panel. 
  onHidden(){
    this.show = false;
    this.load = false;
    this.user.avatar_url = '';
  }
  //  function that is called at startup
  ngOnInit():void {
  {
    this.login = '';
    this.show = false;
    this.load = false
  }

}
}

